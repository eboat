###############################################################################
# BRLTTY - A background process providing access to the console screen (when in
#          text mode) for a blind person using a refreshable braille display.
#
# Copyright (C) 1995-2007 by The BRLTTY Developers.
#
# BRLTTY comes with ABSOLUTELY NO WARRANTY.
#
# This is free software, placed under the terms of the
# GNU General Public License, as published by the Free Software
# Foundation.  Please see the file COPYING for details.
#
# Web Page: http://mielke.cc/brltty/
#
# This software is maintained by Dave Mielke <dave@mielke.cc>.
###############################################################################

# U.S. English Grade 2 Braille Contraction Table
# Created by John Boyer <director@chpi.org>.

numsign 3456  number sign, just one operand
letsign 56
capsign 6
begcaps 6-6
endcaps 6-3
# If you don't want capitalization comment out the preceding two lines.

# the decimal digits
always 1 1
always 2 12
always 3 14
always 4 145
always 5 15
always 6 124
always 7 1245
always 8 125
always 9 24
always 0 245

# the letter a
lastlargesign a 1
always a 1
always A 1
always about 1-12
contraction ab
always above 1-12-1236
contraction abv
always according 1-14
contraction ac
always across 1-14-1235
contraction acr
always ae =
endword aed 1-1246
begword aforem 1-123456-15-134 aforementioned 
word aforesaid 1-123456-15-234-145
always after 1-124
contraction af
sufword afternoon 1-124-1345 afternoons
contraction afn
always afterward 1-124-2456
contraction afw
word again 1-1245
contraction ag
word against 1-1245-34
contraction agst
endword agery = orangery
midendword ally 6-13456
word almost 1-123-134
contraction alm
word already 1-123-1235
contraction alr
word also 1-123
contraction al
word although 1-123-1456
word alth =
word altogether 1-123-2345
contraction alt
word always 1-123-2456
contraction alw
midendword ance 46-15
largesign and 12346
midword angh = Shanghai
begword antenn 1-1345-2345-26-1345 antenna
begword anterio 1-1345-2345-12456-24-135 anterior
begword aqued = aqueduct
always ar 345
word aright 1-5-1235
word as 1356
contraction as
midendword ation 6-1345
midendword aunder 1-136-1345-145-12456 saunders
begword auto = autofocus

# the letter b
always b 12
always B 12
midword bb 23
midendword bble 12-3456
lowword be 23
begword be 23
begword bea 12-2
always bear 12-15-345
begword beatif 23-1-2345-24-124
begword beatit 23-1-2345-24-2345
begword beatr 23-1-2345-1235 Beatrice
always beck 12-15-14-13
always bed 12-1246
begword beda 23-145-1 bedazzle
begword bede 23-145-15 bedevil
word bede 12-1246-15 bede (a name)
begword bedi 23-145-24 bedizzened
begword bedra 23-145-1235-1 bedraggled
word bee 12-15-15
begword bee 12-15-15
always been 12-15-26
always beer 12-15-12456
word beg =
begword begg 12-15-2356
word begs =
begword bei = beijing
always being 23-346
sufword belch 12-15-123-16
begword beld = Beldon
begword belf = belfry, belfast
begword belg = Belgium
begword belk = Belkin
always bell =
begword belm = Belmont
always belsh 12-15-123-146
always belt =
begword belv = belvedere
begword belw = belwether
always ben 12-26
word benefic 23-1345-15-124-24-14
sufword beneficence 23-1345-15-124-24-14-56-15
sufword beneficent 23-1345-15-124-24-14-26-2345
begword benev 23-1345-15-1236 benevolent
begword benig 23-1345-24-1245 benign
begword benigh 23-1345-24-126 benighted
begword benu 23-1345-136 benumbed
begword ber 12-12456
begword bera 23-1235-1 berating
begword bere 23-1235-15 bereft
begword berea 23-1235-2 bereaved
begword berh 23-1235-125 berhymed
begword beribb 23-1235-24-23 beribboned
begword bering 23-1235-346 beringed (assume Bering with no suffix refers to the Sea and use "er")
begword bero 23-1235-135 berobed
always best 12-15-34
sufword bestow 23-34-246
always beth 12-15-1456 Bethany
always bethe 12-15-2346 Bethel (Hans) Bethe
sufword bethink 23-1456-35-13
sufword bethought 23-1456-5-1256
always bets =
always bett =
sufword bev = bevies
begword bever 12-5-15 Beverly beverage
always bio =
midendword bious 12-24-1256-234 dubious
joinword by 356
word by-and-by 12-13456-36-12346-36-12-13456
always because 23-14
word bec =
always before 23-124
word bef =
always behind 23-125
word behring 12-15-125-1235-346
word beh =
word bel =
begword beln = Belnick
always below 23-123
always beneath 23-1345
always beside 23-234
word bes =
sufword bess = Bessie
always between 23-2345
word bet =
always beyond 23-13456
word bey =
midendword ble 3456
midendword bleau 12-123-2-136 tableau
midendword bleed 12-123-15-1246 nosebleed
always bless 12-46-234 joblessness
always blind 12-123
contraction bl
always blinded 12-123-35-145-1246
sufword blinder 12-123-35-145-12456
word blinding 12-123-35-145-346
always braille 12-1235-123
contraction brl
word but 12

# the letter c
always c 14
always C 14
begword cart 14-345-2345 carthorse
midword cch 14-16
always chloro 16-123-135-1235-135
sufword citron = citronella
sufword coed 14-135-1246
begword cofac = cofactor
always cofound 14-135-124-46-145
begword com 36
sufword common 36-134-135-1345 commonest
begword con 25
sufword conceive 25-14-1236
word concv =
word concvd =
word concvr =
word concvs =
word concvst =
word concvth =
sufword conceiving 25-14-1236-1245
word concvg =
always cone 14-5-135
sufword colonel =
sufword conelrad =
word cons 14-135-1345-234
sufword conundrum = 
sufword coronel =
always could 14-145
contraction cd
sufword coworker 14-135-5-2456-12456
midword cc 25
word can 14
always cannot 456-14
word ch =
always ch 16
always character 5-16
always chemo 16-15-134-135
word child 16
always children 16-1345
word chn =
sufword clever 14-123-5-15 cleverest

# the letter d
always d 145
always D 145
begword dachs 145-1-16-234 dachshund
begword dared 145-345-15-145 daredevil
always day 5-145
begword deact = deactivation
begword deall = deallocate
begword decarb 145-15-14-345-12
always deceive 145-14-1236
contraction dcv
contraction dcvd
contraction dcvr
contraction dcvs
word dcvst =
word dcvth =
always deceiving 145-14-1236-1245
contraction dcvg
always declare 145-14-123
contraction dcl
contraction dcld
contraction dclr
contraction dcls
word dclst =
word dclth =
always declaring 145-14-123-1245
contraction dclg
begword deref = dereferencing
begword dereg = deregulation
midword dd 256
midendword dday 145-5-145 midday
begword dedic 145-1246-24-14 dedicated
always dedu = nondeductible
begword deno = denote
always denom =
begword denou 145-15-1345-1256 denounce
begword denu = denunciation
begword dera = derail
begword deri 145-15-1235-24
begword dero = derogatory
begword dinu 145-24-1345-136
begword dis 256
word disc =
word discs =
always dish 145-24-146
begword disha 256-125-1 dishabile
begword dishear 256-125-15-345 disheartened
begword disho 256-125-135 dishonor
begword dishone 256-125-5-135 dishonest
sufword disk =
sufword dispirit 145-24-456-234 dispirited
begword disul = disulfide
word do 145
always dumb = dumbbell

# the letter e
always e 15
always E 15
midword ea 2
midword eabil = interchangeability
always eable 15-1-3456
endword eably = noticeably
midendword eage = mileage
midendword eager 2-1245-12456 meager
always eally 15-6-13456
midendword eance 15-46-15 vengeance
midendword eand 15-12346 meander
always eation 15-6-1345
always ear 15-345
sufword hideaw = hideaway
always ed 1246
always edic = edict Benedict
sufword edition 15-145-24-56-1345 editions
midword edo 15-145-135
always edraw =
word either 15-24
contraction ei
word en =
always en 26
always ename 15-5-1345
sufword enamel 26-1-134-15-123 enameled
midendword ence 56-15
always enceph 26-14-15-1234-125 electroencephalogram
sufword endow 26-145-246
always eneck = bottleneck
midendword eness 15-56-234 closeness
begword enor = enormous
begword enou 15-1345-1256
lowword enough 26
word enough 26-1256-126
begword enu 15-1345-136
always er 12456
begword era =
word eras 12456-1-234
begword erec = erect 
begword ero 15-1235-135
midendword eroom = storeroom
begword eru = erupt
always ethole 15-2345-125-135-123-15 bullethole
always ever 5-15
midendword evere 15-1236-12456-15 Everest
always evered 15-1236-12456-1246
word every 15

# the letter f
always f 124
always F 124
sufword falcon = falconer
midword ff 235
always father 5-124
always fein 124-15-35 Feingold
always first 124-34
word fst =
always fever 124-15-1236-12456
always ffor 124-123456
largesign for 123456
always fore 123456-15
begword forens 123456-26-234 forensic
always forever 123456-5-15 forevermore
always foot =
word from 124
always friend 124-1235
contraction fr
midendword ful 56-123
always funder 124-136-1345-145-12456

# the letter g
always g 1245
always G 1245
begword geo = geoengineering
sufword geoff 1245-15-12356-124 Geoffrey
midword gg 2356
always gh 126
endword gham = Langham
always ghead 1245-125-2-145
always gheart 1245-125-15-345-2345
midendword ghill = dunghill
midendword ghorn = bighorn
always ghouse 1245-125-1256-234-15
always ghz = (gigahertz)
begword givea = giveaway
sufword gnome = gnomedb
word go 1245
always good 1245-145
contraction gd
begword gos = goshawk
word goshen 1245-135-146-26
always great 1245-1235-2345
contraction grt
sufword guenever 1245-136-26-15-1236-12456

# the letter h
always h 125
always H 125
always had 456-125
begword hadd 125-1-256 haddock
sufword hade = hadean 
sufword hadr = hadrian 
word have 125
always headd 125-2-145-145 headdress
always here 5-125
always hered 125-12456-1246
always heren 125-12456-26
midendword herence 125-12456-56-15 adherence
always herer 125-12456-12456
always heres 125-12456-15-234
always heret 125-12456-15-2345
word heretofore 5-125-2345-135-123456-15
word herself 125-12456-124
word herf =
word him 125-134
word hm 125-3-134
sufword hmm =
word himself 125-134-124
contraction hmf
lowword his 236
sufword horse = horseradish
always hydro =

# the letter i
always i 24
always I 24
word i 24
midendword iever 24-15-1236-12456
always immediate 24-134-134
contraction imm
begword immuno = immunofluorescence
lowword in 35
word in =
always in 35
begword incon 35-14-135-1345 incongruous
begword indis 35-145-24-234 indistinct
always iness 24-56-234
midendword ing 346
midword inga 35-1245-1 nightingale
always ingar 35-1245-345 Weingarten
midword ingi 35-1245-24 meningitis
midendword inging 346-346 bringing
joinword into 35-235
always isomer 24-234-135-134-12456
word it 1346
word its 1346-234
contraction xs
word itself 1346-124
contraction xf
midendword ity 56-13456

# the letter j
always j 245
always J 245
word just 245

# the letter k
always k 13
always K 13
always know 5-13
word knowledge 13

# the letter l
always l 123
always L 123
midendword less 46-234
always letter 123-1235
contraction lr
word like 123
always little 123-123
contraction ll
always lord 5-123
word lucknow 123-136-14-13-1345-246

# the letter m
always m 134
always M 134
always many 456-134
begword mc =
always medic 134-1246-24-14 medicare
midendword ment 56-2345
midword menth 134-26-1456 Blumenthal
always mideast 134-24-145-15-1-34
word milling 134-24-123-123-346
begword mishand 134-24-234-125-12346 mishandled
always mishap =
sufword mishear 134-24-234-125-15-345 misheard
begword missh 134-24-234-146 misshapen
word mistook =
begword mistran = mistranslation
sufword mistreat 134-24-234-2345-1235-2-2345
begword mistru = mistrust
begword misty = mistyped
word monetary 134-5-135-2345-345-13456
word more 134
always mother 5-134
always much 134-16
word mch =
word must 134-34
word mst =
begword musti 134-34-24 mustiness
word mustn 134-34-1345 mustn't
word musty 134-34-13456
begword myo = myofibroblasts
word myself 134-13456-124
contraction myf

# the letter n
always n 1345
always N 1345
always name 5-1345
always nament 1345-1-56-2345 tournament
always namese =
endnum nd = (second)
always necessary 1345-15-14
contraction nec
word neither 1345-15-24
contraction nei
midendword ness 56-234
always nighth 1345-24-126-2345-125 nighthawk knighthood
begword non =
word none 1345-5-135
word nones 1345-5-135-234
word nonesuch 1345-5-135-234-16
word nonetheless 1345-5-135-2346-46-234
word noone 1345-135-5-135
sufword nose = nosedive
word not 1345
word noways =
word nowhere 1345-135-5-156
begword nuth 1345-136-2345-125 nuthatch nuthouse

# the letter o
word o 135
always o 135
word O 135
always O 135
begword oe = Oedipus
midword oed = Schroeder
always oen = Phoenix
largesign of 12356
midendword ofar 135-124-345 insofar
always ofold = twofold
midword ofor 135-123456
always onesi =
midendword oness 135-56-234
midendword oneer 135-1345-15-12456
midendword oned 135-1345-1246
always one 5-135
always onem = phoneme
midendword onement 5-135-56-2345 atonement
always onent 135-1345-26-2345
midendword oneous 135-1345-15-1256-234 erroneous
always oner 135-1345-12456
midendword onese = Cantonese,
word oneself 5-135-124
word onef =
midendword oness 135-56-234 Deaconess
midendword onet = phonetics bayonet
endword onez = Ordonez
midendword ong 56-1245
midendword ongen 135-1345-1245-26 uncongenial
always oon = sooner
word ou =
always ou 1256
midendword ound 46-145
midendword ount 46-2345
always ourselves 1256-1235-1236-234
word ourvs =
word out 1256
always ought 5-1256
always ow 246
word o'clock 135-3-14

# the letter p
always p 1234
always P 1234
always paid 1234-145
contraction pd
begword palingen 1234-1-123-35-1245-26 palingenesis (new birth)
always part 5-1234
begword parta 1234-345-2345-1 partake
begword parto 1234-345-2345-135
begword peacen 1234-2-14-15-1345 peacenik
word people 1234
always perceive 1234-12456-14-1236
word percv =
word percvd =
word percvr =
word percvs =
word percvst =
word percvth =
always perceiving 1234-12456-14-1236-1245
word percvg =
always perhaps 1234-12456-125
word perh =
sufword perseverance 1234-12456-234-15-1236-12456-46-15
sufword persevere 1234-12456-234-15-1236-12456-15
sufword persevered 1234-12456-234-15-1236-12456-1246
sufword perseverer 1234-12456-234-15-1236-12456-12456
sufword persevering 1234-12456-234-15-1236-12456-346
always pher 1234-125-12456 cyphered
begword pinea 1234-35-15-1 pineapple
sufword poleax =
begword portho = porthole
begword potho = pothole pothook
begword pre =
always preach 1234-1235-2-16
sufword predator 1234-1235-1246-1-2345-135-1235
always prof =
begword proff 1234-1235-12356-124 proffer
always profit 1234-1235-12356-24-2345

# the letter q
always q 12345
always Q 12345
always quick 12345-13
contraction qk
word quite 12345
always question 5-12345

# the letter r
always r 1235
always R 1235
sufword rafter 1235-1-124-2345-12456
word rather 1235
sufword raw = rawhide
endnum rd = (third)
begword reab = reabsorbed
always reach 1235-2-16
always react =
always reaction 1235-15-1-14-56-1345
begword readj = readjust
begword readm = readmit
begword reaff 1235-15-1-235
always reagent 1235-15-1-1245-26-2345
begword reagg 1235-15-1-2356 reaggregated
begword realig = realign
begword rean = reanalyze, reanimate
begword reapp = reappear
begword reasc 1235-15-1-234-14 reascend
begword reass 1235-15-1-234-234
begword reatt = reattach
begword reau = reauthorization
begword reaw = reawaken
begword rede =
begword redi =
word redo =
begword redol 1235-1246-135-123 redolent
word redone 1235-15-145-5-135
begword redou 1235-15-145-1256 redouble redoubt 
always redu = reduce redundant reduplicate
always redul 1235-1246-136-123 incredulous
always receive 1235-14-1236
contraction rcv
contraction rcvd
contraction rcvr
contraction rcvs
word rcvst =
word rcvth =
always receiving 1235-14-1236-1245
contraction rcvg
begword redis = redistribute
begword redr = redress
always rejoice 1235-245-14
contraction rjc
contraction rjcd
contraction rjcr
contraction rjcs
word rjcst =
word rjcth =
always rejoicing 1235-245-14-1245
contraction rjcg
sufword renaming 1235-15-1345-1-134-346 renamings
begword rene = renegotiate
begword renegad 1235-26-15-1245-1-145 renegade
begword renom = renominate
begword renou 1235-15-1345-1256 renounce
sufword renown 1235-15-1345-246-1345 renowned
begword renu = renunciation
begword rer 1235-15-1235 reread
always rever 1235-15-1236-12456
always reveren 1235-5-15-26 irreverent
always reverence 1235-5-15-56-15 irreverence
sufword reverie 1235-5-15-24-15
always right 5-1235

# the letter s
endnum s = 40s (no letter sign)
always s 234
always S 234
word said 234-145
contraction sd
sufword salmon = salmonella
sufword saw = sawhorse
sufword screw = screwhole
always seda =
always sedation 234-15-145-6-1345
always sedativ 234-1246-1-2345-24-1236
always sedi =
always sediment 234-1246-24-56-2345
always sedu =
begword sedul 234-1246-136-123 sedulous
sufword sedum 234-1246-136-134
always sent 234-26-2345 sentimental
sufword severe 234-15-1236-12456-15
always severed 234-5-15-1246
always severer 234-5-15-12456
always severit 234-15-1236-12456-24-2345
always severity 234-15-1236-12456-56-13456
word shall 146
word sh =
always sh 146
always shaus = In German names
always should 146-145
word shd =
always shoulder 146-1256-123-145-12456
word singapore 234-346-1-1234-135-1235-15 
midendword sion 46-1345
midword stak 234-2345-1-13
midendword stion 234-56-1345
word so 234
always some 5-234
midendword somed 234-135-134-1246 ransomed
always somer 234-135-134-12456 somersault 
always spirit 456-234
midendword ssword 234-234-45-2456 crossword 
endnum st 34 (first)
word st =
always st 34
always sth 234-1456
always sthe 234-2346
midendword sthead 34-125-2-145 masthead
always sthood 34-125-135-135-145 priesthood
word still 34
always stime 234-5-2345
midendword stown 234-2345-246-1345 Pickstown
begword styro 34-13456-1235-135 styrofoam
always shead 234-125-2-145
always sheart 124-125-15-345-2345
always shouse 234-125-1256-234-15
always ssh =
always shood =
word such 234-16
word sch =
always sword =

# the letter t
always t 2345
always T 2345
endnum th 1456 (fourth, fifth, ...)
word th =
always th 1456
always thand 2345-125-12346 shorthand
word that 2345
always theap 2345-125-2-1234 antheap
always thill 2345-125-24-123-123 anthill
word this 1456
word thyself 1456-13456-124
word thyf =
always tnam =
joinword to 235
word today 2345-145
contraction td
word tomorrow 2345-134
contraction tm
word tonight 2345-1345
contraction tn
always thead 2345-125-2-145
always theast 1456-15-1-34
always theart 2345-125-15-345-2345
always thouse 2345-125-1256-234-15
lastlargesign the 2346
word themselves 2346-134-1236-234
word themvs 1456-15-134-1236-234
always their 456-2346
always thence 1456-56-15
always there 5-2346
midendword thereal 2346-1235-2-123 ethereal
word thereupon 5-2346-45-136
always thered 2346-1235-1246
always therer 2346-1235-12456
always theres 2346-1235-15-234 theresa therese 
always thood =
midendword tion 56-1345
always time 5-2345
midendword timed 1245-24-134-1246
midendword timer 2345-24-134-12456
midendword timet = altimeter
word these 45-2346
always through 5-1456
always together 2345-1245-1235
contraction tgr
word those 45-1456
begword trans = transtype

# the letter u
always u 136
always U 136
begword un = unameliorated
begword unb = unblemished
begword undis = undisturbed
begword uneas = unease
begword uneat = uneaten
sufword unful = unfulfilled
begword unmen 136-1345-134-26 unmentioned
word upon 45-136 Dupont
word us 136
always under 5-136
word unsaid 136-1345-234-145

# the letter v
always v 1236
always V 1236
begword vaing 1236-1-35-1245 vainglory
word very 1236
begword vice = viceroy

# the letter w
always w 2456
always W 2456
lowword was 356
word wh =
always wh 156
midendword whart 2456-125-345-2345 Newhart
word which 156
midendword whouse 2456-125-1256-234-15 Newhouse
begword widea = wideawake
word will 2456
lowword were 2356
always where 5-156
word whereupon 5-156-45-136
word wherever 156-12456-5-15
sufword wiseacre =
largesign with 23456
always word 45-2456
word whose 45-156
always work 5-2456
always would 2456-145
contraction wd
always world 456-2456

# the letter x
always x 1346
always X 1346

# the letter y
always y 13456
always Y 13456
word you 13456
always young 5-13456
word your 13456-1235
contraction yr
word yours 13456-1235-234
contraction yrs
word yourself 13456-1235-124
contraction yrf
word yourselves 13456-1235-1236-234
contraction yrvs

# the letter z
always z 1356
always Z 1356

always agosom = phagosome
always ibosom = ribosome
always omosom = chromosome

# Syst�me International Prefixes
begword yotta 13456-135-2345-2345-1 10^24
begword zetta 1356-15-2345-2345-1 10^21
# begword exa 15-1346-1 10^18
begword peta 1234-15-2345-1 10^15
begword tera 2345-12456-1 10^12
begword giga 1245-24-1245-1 10^9
begword mega 134-15-1245-1 10^6
begword kilo 13-24-123-135 10^3
begword hecto 125-15-14-2345-135 10^2
begword deca 145-15-14-1 10^1
begword deci 145-15-14-24 10^-1
begword centi 14-26-2345-24 10^-2
begword milli 134-24-123-123-24 10^-3
begword micro 134-24-14-1235-135 10^-6
begword nano 1345-1-1345-135 10^-9
begword pico 1234-24-14-135 10^-12
begword femto 124-15-134-2345-135 10^-15
begword atto 1-2345-2345-135 10^-18
begword zepto 1356-15-1234-2345-135 10^-21
begword yocto 13456-135-14-2345-135 10^-24

begword ante =
begword anti =
begword endo 26-145-135
begword epi =
begword extra =
begword hyper 125-13456-1234-12456
begword hypo =
begword infra 35-124-1235-1
begword inter 35-2345-12456
begword intra 35-2345-1235-1
begword iso =
begword macro =
begword meta =
begword micro =
begword mono =
begword multi =
begword patho 1234-1-1456-135
begword peri 1234-12456-24
begword poly =
begword post 1234-135-34
begword pre =
begword pseudo =
begword retro =
# begword semi = seminar
begword sub =
begword super 234-136-1234-12456
begword tetra =
begword trans =
begword ultra =
# begword uni =

# other prefixes
begword electro =
begword gastro 1245-1-34-1235-135
begword neuro =
begword psycho 1234-234-13456-16-135

prepunc " 236
postpunc " 356
always " 5

postpunc '' 356
postpunc ''' 3-356
always ' 3
word 'em = 
word 'tis =
word 'twas =
word 'twill =
endword 'd 3-145
endword 'll 3-123-123
endword 'm 3-134
endword 're 3-1235-15
endword 's 3-234
endword 't 3-2345
endword 've 3-1236-15

prepunc `` 236
always ` 4

midnum ^ 45
always ^ 456-126

always ~ 4-156
repeated ~~~ 4-156-4-156-4-156

midnum , 2
always , 2

always ; 23

midnum : 25
always : 25
repeated ::: 25-25-25

midnum . 46
always . 256
always ... 3-3-3
always .\s.\s. 3-3-3 . . .

endnum ! 12346 (factorial)
always ! 235

always ? 236

always ( 2356
always ) 2356

always [ 456-12356
always ] 456-23456

always { 46-12356
always } 46-23456

always # 3456

midnum * 4-16
always * 35-35
repeated *** 16-16-16

midnum / 34
always / 456-34

always % 4-356

always & 456-12346

always @ 4-1

always \\ 456-16

always | 456-1256

repeated \s 0
repeated \t 0
repeated \xa0 0 no break space

always -com =
repeated --- 36-36-36
always \s-\s 36-36
always \s-\scom 36-36-14-135-134

always _ 78
repeated ___ 78-78-78

repeated === 46-13-46-13-46-13

# the hyphen
always � 36
repeated ��� 36-36-36
always \s�\s 36-36

# accented letters
always � 6-4-1      [C0] upper a grave
always � 6-4-1      [C1] upper a acute
always � 6-4-1      [C2] upper a circumflex
always � 6-4-1      [C3] upper a tilde
always � 6-4-1      [C4] upper a dieresis
always � 6-4-1      [C5] upper a ring
always � 6-1-15     [C6] upper ae
always � 6-4-14     [C7] upper c cedilla
always � 6-4-15     [C8] upper e grave
always � 6-4-15     [C9] upper e acute
always � 6-4-15     [CA] upper e circumflex
always � 6-4-15     [CB] upper e dieresis
always � 6-4-24     [CC] upper i grave
always � 6-4-24     [CD] upper i acute
always � 6-4-24     [CE] upper i circumflex
always � 6-4-24     [CF] upper i dieresis
always � 6-4-15     [D0] upper eth
always � 6-4-1345   [D1] upper n tilde
always � 6-4-135    [D2] upper o grave
always � 6-4-135    [D3] upper o acute
always � 6-4-135    [D4] upper o circumflex
always � 6-4-135    [D5] upper o tilde
always � 6-4-135    [D6] upper o dieresis
always � 6-4-135    [D8] upper o slash
always � 6-4-136    [D9] upper u grave
always � 6-4-136    [DA] upper u acute
always � 6-4-136    [DB] upper u circumflex
always � 6-4-136    [DC] upper u dieresis
always � 6-4-13456  [DD] upper y acute
always � 6-4-2345   [DE] upper t horn
always � 234-234    [DF] lower ss
always �   4-1      [E0] lower a grave
always �   4-1      [E1] lower a acute
always �   4-1      [E2] lower a circumflex
always �   4-1      [E3] lower a tilde
always �   4-1      [E4] lower a dieresis
always �   4-1      [E5] lower a ring
always �   1-15     [E6] lower ae
always �   4-14     [E7] lower c cedilla
always �   4-15     [E8] lower e grave
always �   4-15     [E9] lower e acute
always �   4-15     [EA] lower e circumflex
always �   4-15     [EB] lower e dieresis
always �   4-24     [EC] lower i grave
always �   4-24     [ED] lower i acute
always �   4-24     [EE] lower i circumflex
always �   4-24     [EF] lower i dieresis
always �   4-15     [F0] lower eth
always �   4-1345   [F1] lower n tilde
always �   4-135    [F2] lower o grave
always �   4-135    [F3] lower o acute
always �   4-135    [F4] lower o circumflex
always �   4-135    [F5] lower o tilde
always �   4-135    [F6] lower o dieresis
always �   4-135    [F8] lower o slash
always �   4-136    [F9] lower u grave
always �   4-136    [FA] lower u acute
always �   4-136    [FB] lower u circumflex
always �   4-136    [FC] lower u dieresis
always �   4-13456  [FD] lower y acute
always �   4-2345   [FE] lower t horn
always �   4-13456  [FF] lower y dieresis

# mathematical symbols
always < 5-13
always = 46-13
always > 46-2
midnum + 346
always + 346
midnum - 36
always - 36
always � 46-16 multiplication sign
midnum � 46-34 division sign
begnum $ 256
always $ 256-3456

# other special characters
always � 2356-6-14-2356 copyright
always � 4-1234-345 paragraph
always � 4-234-3 section
always � 45-46-16 degrees
always � 4-14 cents
always � 4-123 pounds
always � 4-13456 yen
always � 46-134 mu

# special character sequences
literal :// URLs
literal www.

literal .com
literal .edu
literal .gov
literal .mil
literal .net
literal .org
include countries.cti

literal .doc
literal .htm
literal .html
literal .tex
literal .txt

literal .gif
literal .jpg
literal .png
literal .wav

literal .tar
literal .zip

# d,g,r,rs,s,st,th conceive deceive declare perceive receive rejoice
# n't could must should would

# When an upper-case letter occurs inside a contraction, following a lower-case
# letter, the contraction should not be used. Example McCan

# Windows mail programs use all sorts of weird characters for apostrophes. 
# I would prefer to have all characters above 126 just show up as a 
# backslash and two hex digits.

# problems with quotation marks before and after dashes: "division"--a

# when a decimal begins with a period, it should be translated with a 
# number sign followed by a decimal point, followed by the number.
