###############################################################################
# BRLTTY - A background process providing access to the console screen (when in
#          text mode) for a blind person using a refreshable braille display.
#
# Copyright (C) 1995-2007 by The BRLTTY Developers.
#
# BRLTTY comes with ABSOLUTELY NO WARRANTY.
#
# This is free software, placed under the terms of the
# GNU General Public License, as published by the Free Software
# Foundation.  Please see the file COPYING for details.
#
# Web Page: http://mielke.cc/brltty/
#
# This software is maintained by Dave Mielke <dave@mielke.cc>.
###############################################################################

# BRLTTY Text Translation Table - VisioBraille (iso-8859-1)

# This table is the default one for VisioBraille braille terminals.
# It was first used by Sagem printers, before being adopted by Handialog as
# the default braille table.
# Although it is a bit old, VisioBraille users may find this table more
# comfortable than standard French braille tables.

\X00 (  345 78)  # null
\X01 (1      8)  # start of heading
\X02 (12     8)  # start of text
\X03 (1  4   8)  # end of text
\X04 (1  45  8)  # end of transmission
\X05 (1   5  8)  # enquiry
\X06 (12 4   8)  # acknowledge
\X07 (12 45  8)  # bell
\X08 (12  5  8)  # backspace
\X09 ( 2 4   8)  # horizontal tabulation
\X0A ( 2 45  8)  # line feed
\X0B (1 3    8)  # vertical tabulation
\X0C (123    8)  # form feed
\X0D (1 34   8)  # carriage return
\X0E (1 345  8)  # shift out
\X0F (1 3 5  8)  # shift in
\X10 (1234   8)  # data link escape
\X11 (12345  8)  # device control one
\X12 (123 5  8)  # device control two
\X13 ( 234   8)  # device control three
\X14 ( 2345  8)  # device control four
\X15 (1 3  6 8)  # negative acknowledge
\X16 (123  6 8)  # synchronous idle
\X17 ( 2 456 8)  # end of transmission block
\X18 (1 34 6 8)  # cancel
\X19 (1 3456 8)  # end of medium
\X1A (1 3 56 8)  # substitute
\X1B (  345678)  # escape
\X1C (1    6 8)  # file separator
\X1D (12   6 8)  # group separator
\X1E (1  4 6 8)  # record separator
\X1F (1  456 8)  # unit separator
\X20 (        )  # space
\X21 ( 23 5   )  # exclamation mark
\X22 (   4    )  # quotation mark
\X23 (   4 6  )  # number sign
\X24 (   456  )  # dollar sign
\X25 (   45   )  # percent sign
\X26 (  345   )  # ampersand
\X27 (  3     )  # apostrophe
\X28 (     6  )  # left parenthesis
\X29 (  3 5   )  # right parenthesis
\X2A (  34    )  # asterisk
\X2B (    56  )  # plus sign
\X2C ( 2      )  # comma
\X2D (  3  6  )  # hyphen-minus
\X2E ( 2  56  )  # full stop
\X2F (123456  )  # solidus
\X30 (  3456  )  # digit zero
\X31 (1    6  )  # digit one
\X32 (12   6  )  # digit two
\X33 (1  4 6  )  # digit three
\X34 (1  456  )  # digit four
\X35 (1   56  )  # digit five
\X36 (12 4 6  )  # digit six
\X37 (12 456  )  # digit seven
\X38 (12  56  )  # digit eight
\X39 ( 2 4 6  )  # digit nine
\X3A ( 2  5   )  # colon
\X3B ( 23     )  # semicolon
\X3C ( 23  6  )  # less-than sign
\X3D ( 23 56  )  # equals sign
\X3E (  3 56  )  # greater-than sign
\X3F ( 2   6  )  # question mark
\X40 (1234 6  )  # commercial at
\X41 (1     7 )  # latin capital letter a
\X42 (12    7 )  # latin capital letter b
\X43 (1  4  7 )  # latin capital letter c
\X44 (1  45 7 )  # latin capital letter d
\X45 (1   5 7 )  # latin capital letter e
\X46 (12 4  7 )  # latin capital letter f
\X47 (12 45 7 )  # latin capital letter g
\X48 (12  5 7 )  # latin capital letter h
\X49 ( 2 4  7 )  # latin capital letter i
\X4A ( 2 45 7 )  # latin capital letter j
\X4B (1 3   7 )  # latin capital letter k
\X4C (123   7 )  # latin capital letter l
\X4D (1 34  7 )  # latin capital letter m
\X4E (1 345 7 )  # latin capital letter n
\X4F (1 3 5 7 )  # latin capital letter o
\X50 (1234  7 )  # latin capital letter p
\X51 (12345 7 )  # latin capital letter q
\X52 (123 5 7 )  # latin capital letter r
\X53 ( 234  7 )  # latin capital letter s
\X54 ( 2345 7 )  # latin capital letter t
\X55 (1 3  67 )  # latin capital letter u
\X56 (123  67 )  # latin capital letter v
\X57 ( 2 4567 )  # latin capital letter w
\X58 (1 34 67 )  # latin capital letter x
\X59 (1 34567 )  # latin capital letter y
\X5A (1 3 567 )  # latin capital letter z
\X5B (123 56  )  # left square bracket
\X5C ( 234 6  )  # reverse solidus
\X5D ( 23456  )  # right square bracket
\X5E (  34 6  )  # circumflex accent
\X5F (  3    8)  # low line
\X60 (  3   7 )  # grave accent
\X61 (1       )  # latin small letter a
\X62 (12      )  # latin small letter b
\X63 (1  4    )  # latin small letter c
\X64 (1  45   )  # latin small letter d
\X65 (1   5   )  # latin small letter e
\X66 (12 4    )  # latin small letter f
\X67 (12 45   )  # latin small letter g
\X68 (12  5   )  # latin small letter h
\X69 ( 2 4    )  # latin small letter i
\X6A ( 2 45   )  # latin small letter j
\X6B (1 3     )  # latin small letter k
\X6C (123     )  # latin small letter l
\X6D (1 34    )  # latin small letter m
\X6E (1 345   )  # latin small letter n
\X6F (1 3 5   )  # latin small letter o
\X70 (1234    )  # latin small letter p
\X71 (12345   )  # latin small letter q
\X72 (123 5   )  # latin small letter r
\X73 ( 234    )  # latin small letter s
\X74 ( 2345   )  # latin small letter t
\X75 (1 3  6  )  # latin small letter u
\X76 (123  6  )  # latin small letter v
\X77 ( 2 456  )  # latin small letter w
\X78 (1 34 6  )  # latin small letter x
\X79 (1 3456  )  # latin small letter y
\X7A (1 3 56  )  # latin small letter z
\X7B (     67 )  # left curly bracket
\X7C ( 23 5  8)  # vertical line
\X7D (  3 5  8)  # right curly bracket
\X7E (    5 78)  # tilde
\X7F (        )  # delete
\X80 (1234 678)  # <control>
\X81 (12  56 8)  # <control>
\X82 (        )  # break permitted here
\X83 (1    6 8)  # no break here
\X84 (  345  8)  # <control>
\X85 (123 56 8)  # next line
\X86 (        )  # start of selected area
\X87 (1234 6 8)  # end of selected area
\X88 (12   6 8)  # character tabulation set
\X89 (12 4 6 8)  # character tabulation with justification
\X8A ( 234 6 8)  # line tabulation set
\X8B (12 456 8)  # partial line down
\X8C (1  4 6 8)  # partial line up
\X8D (        )  # reverse line feed
\X8E (  345 78)  # single shift two
\X8F (        )  # single shift three
\X90 (12345678)  # device control string
\X91 (        )  # private use one
\X92 (        )  # private use two
\X93 (1  456 8)  # set transmit state
\X94 ( 2 4 6 8)  # cancel character
\X95 (        )  # message waiting
\X96 (1   56 8)  # start of guarded area
\X97 ( 23456 8)  # end of guarded area
\X98 (12 456 8)  # start of string
\X99 ( 2 4 678)  # <control>
\X9A (12  5678)  # single character introducer
\X9B (        )  # control sequence introducer
\X9C (        )  # string terminator
\X9D (        )  # operating system command
\X9E (        )  # privacy message
\X9F (        )  # application program command
\XA0 (        )  # no-break space
\XA1 (        )  # inverted exclamation mark
\XA2 (        )  # cent sign
\XA3 (        )  # pound sign
\XA4 (        )  # currency sign
\XA5 (        )  # yen sign
\XA6 (        )  # broken bar
\XA7 (        )  # section sign
\XA8 (        )  # diaeresis
\XA9 (        )  # copyright sign
\XAA (        )  # feminine ordinal indicator
\XAB (        )  # left-pointing double angle quotation mark
\XAC (        )  # not sign
\XAD (        )  # soft hyphen
\XAE (        )  # registered sign
\XAF (        )  # macron
\XB0 (        )  # degree sign
\XB1 (        )  # plus-minus sign
\XB2 (        )  # superscript two
\XB3 (        )  # superscript three
\XB4 (        )  # acute accent
\XB5 (        )  # micro sign
\XB6 (        )  # pilcrow sign
\XB7 (        )  # middle dot
\XB8 (        )  # cedilla
\XB9 (        )  # superscript one
\XBA (        )  # masculine ordinal indicator
\XBB (        )  # right-pointing double angle quotation mark
\XBC (        )  # vulgar fraction one quarter
\XBD (        )  # vulgar fraction one half
\XBE (        )  # vulgar fraction three quarters
\XBF (        )  # inverted question mark
\XC0 (123 5678)  # latin capital letter a with grave
\XC1 (        )  # latin capital letter a with acute
\XC2 (1     78)  # latin capital letter a with circumflex
\XC3 (        )  # latin capital letter a with tilde
\XC4 (  3 5 78)  # latin capital letter a with diaeresis
\XC5 (        )  # latin capital letter a with ring above
\XC6 (        )  # latin capital letter ae
\XC7 (1234 678)  # latin capital letter c with cedilla
\XC8 (        )  # latin capital letter e with grave
\XC9 (12345678)  # latin capital letter e with acute
\XCA (12   678)  # latin capital letter e with circumflex
\XCB (12 4 678)  # latin capital letter e with diaeresis
\XCC (        )  # latin capital letter i with grave
\XCD (        )  # latin capital letter i with acute
\XCE (1  4 678)  # latin capital letter i with circumflex
\XCF (12 45678)  # latin capital letter i with diaeresis
\XD0 (        )  # latin capital letter eth
\XD1 (        )  # latin capital letter n with tilde
\XD2 (        )  # latin capital letter o with grave
\XD3 (        )  # latin capital letter o with acute
\XD4 (1 3 5678)  # latin capital letter o with circumflex
\XD5 (        )  # latin capital letter o with tilde
\XD6 ( 2 4 678)  # latin capital letter o with diaeresis
\XD7 (        )  # multiplication sign
\XD8 (        )  # latin capital letter o with stroke
\XD9 ( 2345678)  # latin capital letter u with grave
\XDA (        )  # latin capital letter u with acute
\XDB (1   5678)  # latin capital letter u with circumflex
\XDC (12  5678)  # latin capital letter u with diaeresis
\XDD (        )  # latin capital letter y with acute
\XDE (        )  # latin capital letter thorn
\XDF (        )  # latin small letter sharp s
\XE0 (123 56 8)  # latin small letter a with grave
\XE1 (        )  # latin small letter a with acute
\XE2 (1    6 8)  # latin small letter a with circumflex
\XE3 (        )  # latin small letter a with tilde
\XE4 (  345  8)  # latin small letter a with diaeresis
\XE5 (        )  # latin small letter a with ring above
\XE6 (        )  # latin small letter ae
\XE7 (1234 6 8)  # latin small letter c with cedilla
\XE8 ( 234 6 8)  # latin small letter e with grave
\XE9 (123456 8)  # latin small letter e with acute
\XEA (12   6 8)  # latin small letter e with circumflex
\XEB (12 4 6 8)  # latin small letter e with diaeresis
\XEC (        )  # latin small letter i with grave
\XED (        )  # latin small letter i with acute
\XEE (1  4 6 8)  # latin small letter i with circumflex
\XEF (12 456 8)  # latin small letter i with diaeresis
\XF0 (        )  # latin small letter eth
\XF1 (        )  # latin small letter n with tilde
\XF2 (        )  # latin small letter o with grave
\XF3 (        )  # latin small letter o with acute
\XF4 (1  456 8)  # latin small letter o with circumflex
\XF5 (        )  # latin small letter o with tilde
\XF6 ( 2 4 6 8)  # latin small letter o with diaeresis
\XF7 (        )  # division sign
\XF8 (        )  # latin small letter o with stroke
\XF9 ( 23456 8)  # latin small letter u with grave
\XFA (        )  # latin small letter u with acute
\XFB (1   56 8)  # latin small letter u with circumflex
\XFC (12  56 8)  # latin small letter u with diaeresis
\XFD (        )  # latin small letter y with acute
\XFE (        )  # latin small letter thorn
\XFF (        )  # latin small letter y with diaeresis
