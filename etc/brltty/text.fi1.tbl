###############################################################################
# BRLTTY - A background process providing access to the console screen (when in
#          text mode) for a blind person using a refreshable braille display.
#
# Copyright (C) 1995-2007 by The BRLTTY Developers.
#
# BRLTTY comes with ABSOLUTELY NO WARRANTY.
#
# This is free software, placed under the terms of the
# GNU General Public License, as published by the Free Software
# Foundation.  Please see the file COPYING for details.
#
# Web Page: http://mielke.cc/brltty/
#
# This software is maintained by Dave Mielke <dave@mielke.cc>.
###############################################################################

# BRLTTY Text Translation Table - Finnish (iso-8859-1)

# This table is very close to the official Finnish braille character set.

\X00 (   4  78)  # null
\X01 (1     78)  # start of heading
\X02 (12    78)  # start of text
\X03 (1  4  78)  # end of text
\X04 (1  45 78)  # end of transmission
\X05 (1   5 78)  # enquiry
\X06 (12 4  78)  # acknowledge
\X07 (12 45 78)  # bell
\X08 (12  5 78)  # backspace
\X09 ( 2 4  78)  # horizontal tabulation
\X0A ( 2 45 78)  # line feed
\X0B (1 3   78)  # vertical tabulation
\X0C (123   78)  # form feed
\X0D (1 34  78)  # carriage return
\X0E (1 345 78)  # shift out
\X0F (1 3 5 78)  # shift in
\X10 (1234  78)  # data link escape
\X11 (12345 78)  # device control one
\X12 (123 5 78)  # device control two
\X13 ( 234  78)  # device control three
\X14 ( 2345 78)  # device control four
\X15 (1 3  678)  # negative acknowledge
\X16 (123  678)  # synchronous idle
\X17 ( 2 45678)  # end of transmission block
\X18 (1 34 678)  # cancel
\X19 (1 345678)  # end of medium
\X1A (1 3 5678)  # substitute
\X1B (12   678)  # escape
\X1C (12  5678)  # file separator
\X1D (  3 5 78)  # group separator
\X1E ( 2    78)  # record separator
\X1F (    5 78)  # unit separator
\X20 (        )  # space
\X21 ( 2  56  )  # exclamation mark
\X22 (    56  )  # quotation mark
\X23 (  3456  )  # number sign
\X24 ( 234 6  )  # dollar sign
\X25 (1  456  )  # percent sign
\X26 (1234 6  )  # ampersand
\X27 (    5   )  # apostrophe
\X28 ( 23  6  )  # left parenthesis
\X29 (  3 56  )  # right parenthesis
\X2A (  3 5   )  # asterisk
\X2B ( 23 5   )  # plus sign
\X2C ( 2      )  # comma
\X2D (  3  6  )  # hyphen-minus
\X2E (  3     )  # full stop
\X2F (  34    )  # solidus
\X30 ( 2 45  8)  # digit zero
\X31 (1      8)  # digit one
\X32 (12     8)  # digit two
\X33 (1  4   8)  # digit three
\X34 (1  45  8)  # digit four
\X35 (1   5  8)  # digit five
\X36 (12 4   8)  # digit six
\X37 (12 45  8)  # digit seven
\X38 (12  5  8)  # digit eight
\X39 ( 2 4   8)  # digit nine
\X3A ( 2  5   )  # colon
\X3B ( 23     )  # semicolon
\X3C (12   6  )  # less-than sign
\X3D ( 23 56  )  # equals sign
\X3E (1   56  )  # greater-than sign
\X3F ( 2   6  )  # question mark
\X40 (   4    )  # commercial at
\X41 (1     7 )  # latin capital letter a
\X42 (12    7 )  # latin capital letter b
\X43 (1  4  7 )  # latin capital letter c
\X44 (1  45 7 )  # latin capital letter d
\X45 (1   5 7 )  # latin capital letter e
\X46 (12 4  7 )  # latin capital letter f
\X47 (12 45 7 )  # latin capital letter g
\X48 (12  5 7 )  # latin capital letter h
\X49 ( 2 4  7 )  # latin capital letter i
\X4A ( 2 45 7 )  # latin capital letter j
\X4B (1 3   7 )  # latin capital letter k
\X4C (123   7 )  # latin capital letter l
\X4D (1 34  7 )  # latin capital letter m
\X4E (1 345 7 )  # latin capital letter n
\X4F (1 3 5 7 )  # latin capital letter o
\X50 (1234  7 )  # latin capital letter p
\X51 (12345 7 )  # latin capital letter q
\X52 (123 5 7 )  # latin capital letter r
\X53 ( 234  7 )  # latin capital letter s
\X54 ( 2345 7 )  # latin capital letter t
\X55 (1 3  67 )  # latin capital letter u
\X56 (123  67 )  # latin capital letter v
\X57 ( 2 4567 )  # latin capital letter w
\X58 (1 34 67 )  # latin capital letter x
\X59 (1 34567 )  # latin capital letter y
\X5A (1 3 567 )  # latin capital letter z
\X5B (123 56  )  # left square bracket
\X5C (1   4 6 )  # reverse solidus
\X5D ( 23456  )  # right square bracket
\X5E (   45  8)  # circumflex accent
\X5F (      78)  # low line
\X60 (   4   8)  # grave accent
\X61 (1       )  # latin small letter a
\X62 (12      )  # latin small letter b
\X63 (1  4    )  # latin small letter c
\X64 (1  45   )  # latin small letter d
\X65 (1   5   )  # latin small letter e
\X66 (12 4    )  # latin small letter f
\X67 (12 45   )  # latin small letter g
\X68 (12  5   )  # latin small letter h
\X69 ( 2 4    )  # latin small letter i
\X6A ( 2 45   )  # latin small letter j
\X6B (1 3     )  # latin small letter k
\X6C (123     )  # latin small letter l
\X6D (1 34    )  # latin small letter m
\X6E (1 345   )  # latin small letter n
\X6F (1 3 5   )  # latin small letter o
\X70 (1234    )  # latin small letter p
\X71 (12345   )  # latin small letter q
\X72 (123 5   )  # latin small letter r
\X73 ( 234    )  # latin small letter s
\X74 ( 2345   )  # latin small letter t
\X75 (1 3  6  )  # latin small letter u
\X76 (123  6  )  # latin small letter v
\X77 ( 2 456  )  # latin small letter w
\X78 (1 34 6  )  # latin small letter x
\X79 (1 3456  )  # latin small letter y
\X7A (1 3 56  )  # latin small letter z
\X7B (12 4 6  )  # left curly bracket
\X7C (   456  )  # vertical line
\X7D (12 456  )  # right curly bracket
\X7E (   45   )  # tilde
\X7F (1234   8)  # delete
\X80 ( 23  67 )  # <control>
\X81 (12  56  )  # <control>
\X82 (123456  )  # break permitted here
\X83 ( 23  678)  # no break here
\X84 (  345   )  # <control>
\X85 ( 2345  8)  # next line
\X86 (1    6  )  # start of selected area
\X87 (1234567 )  # end of selected area
\X88 (12345678)  # character tabulation set
\X89 (     678)  # character tabulation with justification
\X8A (12345  8)  # line tabulation set
\X8B (  3 56 8)  # partial line down
\X8C (   45  8)  # partial line up
\X8D (   4567 )  # reverse line feed
\X8E (  345 7 )  # single shift two
\X8F (1    67 )  # single shift three
\X90 (1234567 )  # device control string
\X91 (1 34   8)  # private use one
\X92 (12  56 8)  # private use two
\X93 (  3 567 )  # set transmit state
\X94 ( 2 4 6  )  # cancel character
\X95 (     67 )  # message waiting
\X96 (123456 8)  # start of guarded area
\X97 (  3  6 8)  # end of guarded area
\X98 ( 2 456 8)  # start of string
\X99 ( 2 4 67 )  # <control>
\X9A (12  567 )  # single character introducer
\X9B (   4  7 )  # control sequence introducer
\X9C (      7 )  # string terminator
\X9D (   45 7 )  # operating system command
\X9E (1 34 6 8)  # privacy message
\X9F (1 345  8)  # application program command
\XA0 ( 2   678)  # no-break space
\XA1 ( 23 5  8)  # inverted exclamation mark
\XA2 (1 3 56 8)  # cent sign
\XA3 (123    8)  # pound sign
\XA4 (1   5 78)  # currency sign
\XA5 (1 3  6 8)  # yen sign
\XA6 (   4 6 8)  # broken bar
\XA7 (   4 678)  # section sign
\XA8 ( 2  5  8)  # diaeresis
\XA9 (1 3    8)  # copyright sign
\XAA (   4  78)  # feminine ordinal indicator
\XAB ( 23   78)  # left-pointing double angle quotation mark
\XAC (  3 5 7 )  # not sign
\XAD (       8)  # soft hyphen
\XAE (123 5  8)  # registered sign
\XAF ( 23 56 8)  # macron
\XB0 (    56 8)  # degree sign
\XB1 (  3    8)  # plus-minus sign
\XB2 ( 23   7 )  # superscript two
\XB3 ( 2  5 7 )  # superscript three
\XB4 (   4   8)  # acute accent
\XB5 ( 23  6 8)  # micro sign
\XB6 (   4 67 )  # pilcrow sign
\XB7 (  3   78)  # middle dot
\XB8 (  3 5678)  # cedilla
\XB9 ( 2    7 )  # superscript one
\XBA (1 3 5  8)  # masculine ordinal indicator
\XBB (    5678)  # right-pointing double angle quotation mark
\XBC (  3456 8)  # vulgar fraction one quarter
\XBD (  34567 )  # vulgar fraction one half
\XBE (  345678)  # vulgar fraction three quarters
\XBF ( 2   6 8)  # inverted question mark
\XC0 (123 5678)  # latin capital letter a with grave
\XC1 (123 567 )  # latin capital letter a with acute
\XC2 (1    678)  # latin capital letter a with circumflex
\XC3 (1  4 678)  # latin capital letter a with tilde
\XC4 (  345 7 )  # latin capital letter a with diaeresis
\XC5 (1    67 )  # latin capital letter a with ring above
\XC6 (  345 78)  # latin capital letter ae
\XC7 (1234 67 )  # latin capital letter c with cedilla
\XC8 ( 234 678)  # latin capital letter e with grave
\XC9 ( 234 67 )  # latin capital letter e with acute
\XCA (12   67 )  # latin capital letter e with circumflex
\XCB (12 4 67 )  # latin capital letter e with diaeresis
\XCC (  34  78)  # latin capital letter i with grave
\XCD (  34  7 )  # latin capital letter i with acute
\XCE (1  4 67 )  # latin capital letter i with circumflex
\XCF (12 4567 )  # latin capital letter i with diaeresis
\XD0 (1   5678)  # latin capital letter eth
\XD1 (12 45678)  # latin capital letter n with tilde
\XD2 (  34 678)  # latin capital letter o with grave
\XD3 (  34 67 )  # latin capital letter o with acute
\XD4 (1  4567 )  # latin capital letter o with circumflex
\XD5 (1  45678)  # latin capital letter o with tilde
\XD6 ( 2 4 67 )  # latin capital letter o with diaeresis
\XD7 (  3   7 )  # multiplication sign
\XD8 ( 2 4 678)  # latin capital letter o with stroke
\XD9 ( 2345678)  # latin capital letter u with grave
\XDA ( 234567 )  # latin capital letter u with acute
\XDB (1   567 )  # latin capital letter u with circumflex
\XDC (12  567 )  # latin capital letter u with diaeresis
\XDD (1234 678)  # latin capital letter y with acute
\XDE (12 4 6 8)  # latin capital letter thorn
\XDF ( 234   8)  # latin small letter sharp s
\XE0 (123 56 8)  # latin small letter a with grave
\XE1 (123 56  )  # latin small letter a with acute
\XE2 (1    6  )  # latin small letter a with circumflex
\XE3 (1  4 6 8)  # latin small letter a with tilde
\XE4 (  345   )  # latin small letter a with diaeresis
\XE5 (1    6  )  # latin small letter a with ring above
\XE6 (  345  8)  # latin small letter ae
\XE7 (1234 6  )  # latin small letter c with cedilla
\XE8 ( 234 6 8)  # latin small letter e with grave
\XE9 ( 234 6  )  # latin small letter e with acute
\XEA (12   6  )  # latin small letter e with circumflex
\XEB (12 4 6  )  # latin small letter e with diaeresis
\XEC (  34   8)  # latin small letter i with grave
\XED (  34    )  # latin small letter i with acute
\XEE (1  4 6  )  # latin small letter i with circumflex
\XEF (12 456  )  # latin small letter i with diaeresis
\XF0 (1   56 8)  # latin small letter eth
\XF1 (12 456 8)  # latin small letter n with tilde
\XF2 (  34 6 8)  # latin small letter o with grave
\XF3 (  34 6  )  # latin small letter o with acute
\XF4 (1  456  )  # latin small letter o with circumflex
\XF5 (1  456 8)  # latin small letter o with tilde
\XF6 ( 2 4 6  )  # latin small letter o with diaeresis
\XF7 ( 2  567 )  # division sign
\XF8 ( 2 4 6 8)  # latin small letter o with stroke
\XF9 ( 23456 8)  # latin small letter u with grave
\XFA ( 23456  )  # latin small letter u with acute
\XFB (1   56  )  # latin small letter u with circumflex
\XFC (12  56  )  # latin small letter u with diaeresis
\XFD (1234 6 8)  # latin small letter y with acute
\XFE (12 4 678)  # latin small letter thorn
\XFF (1 3456 8)  # latin small letter y with diaeresis
